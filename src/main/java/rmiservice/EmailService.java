package rmiservice;

import model.Email;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class EmailService {

    public Map<String, String> checkEmail(Email email){
        try {
            IEmailValidator emailValidator = (IEmailValidator) Naming.lookup("rmi://localhost:1090/RMIValidator");
            return emailValidator.validate(email);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new HashMap<String, String>();
    }

}
