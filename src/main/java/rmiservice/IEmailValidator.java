package rmiservice;

import model.Email;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

public interface IEmailValidator extends Remote {

    Map<String, String> validate(Email email) throws RemoteException;

}
