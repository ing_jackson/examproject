package socketservice;

import model.Email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailService {


	public Map<String, String> checkEmail(Email email){

			try  {
				Socket socket = new Socket("localhost", 1091);
				BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

				Map<String, String> messages = new HashMap<>();

				StringBuffer outStr = new StringBuffer();

				outStr.append("firstname" + "|" + email.getFirstName() + System.lineSeparator());
				outStr.append("lastname" + "|" + email.getLastName() + System.lineSeparator());
				outStr.append("age" + "|" + email.getAge() + System.lineSeparator());
				outStr.append("phone" + "|" + email.getPhoneNumber() + System.lineSeparator());
				outStr.append("email" + "|" + email.getMailAddress() + System.lineSeparator());
				outStr.append("password" + "|" + email.getPassword() + System.lineSeparator());

				out.print(outStr);
				out.flush();

				List<String> strings = new ArrayList<>();

				String inputLine;

				while ((inputLine = br.readLine()) != null) {
					strings.add(inputLine);
				}

				for (String str : strings) {
					String[] splitStr = str.split("[|]");
					messages.put(splitStr[0], splitStr[1]);
				}

				return messages;

			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return new HashMap<>();
	}
}
