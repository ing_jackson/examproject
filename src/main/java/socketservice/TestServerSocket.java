package socketservice;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestServerSocket {

    public static void main(String args[]) throws IOException {
        ServerSocket serverSocket = new ServerSocket(1091);

        Matcher matcher;

        try {
            while (true) {
                Socket socket = serverSocket.accept();
                socket.setSoTimeout(100);
                try {
                    OutputStream os = socket.getOutputStream();
                    PrintWriter pw = new PrintWriter(os, true);

                    BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    List<String> strings = new ArrayList<>();
                    List<String> messages = new ArrayList<>();

                    while (br.ready()) {
                        strings.add(br.readLine());
                    }

                    for (String str : strings) {
                        String[] splitStr = str.split("[|]");
                        if (splitStr.length == 2) {
                            switch (splitStr[0]) {
                                case "firstname":
                                    matcher = Pattern.compile("^[A-Z]{1}[a-z]*$").matcher(splitStr[1]);
                                    if (!matcher.find()) {
                                        messages.add("firstname" + "|" + "First name is incorrect." + System.lineSeparator());
                                    }
                                    break;
                                case "lastname":
                                    matcher = Pattern.compile("^[A-Z]{1}[a-z]*$").matcher(splitStr[1]);
                                    if (!matcher.find()) {
                                        messages.add("lastname" + "|" + "Last name is incorrect." + System.lineSeparator());
                                    }
                                    break;
                                case "age":
                                    matcher = Pattern.compile("^[\\d]{1,3}$").matcher(splitStr[1]);
                                    if (!matcher.find()) {
                                        messages.add("age" + "|" + "Age is incorrect." + System.lineSeparator());
                                    }
                                    break;
                                case "phone":
                                    matcher = Pattern.compile("[\\d]{3}-[\\d]{1}-[\\d]{2}-[\\d]{2}").matcher(splitStr[1]);
                                    if (!matcher.find()) {
                                        messages.add("phone" + "|" + "Phone number is incorrect." + System.lineSeparator());
                                    }
                                    break;
                                case "email":
                                    matcher = Pattern.compile("^[A-Za-z]{1}[A-Za-z\\d]*\\.[A-Za-z\\d]+@[A-Za-z\\d]{1,10}\\.[A-Za-z]{2,3}$").matcher(splitStr[1]);
                                    if (!matcher.find()) {
                                        messages.add("email" + "|" + "E-mail address is incorrect." + System.lineSeparator());
                                    }
                                    break;
                                case "password":
                                    matcher = Pattern.compile("^(?=(.*?[A-Z]){2,})(?=(.*[a-z]){3,})(?=(.*[\\d]){2,})(?=(.*[\\-\\!\\$\\%\\^\\&\\*\\(\\)\\_\\+\\|\\~\\=\\`\\{\\}\\[\\]\\:\\\"\\;\\'\\<\\>\\?\\,\\.\\/]){3,}).{10,}$").matcher(splitStr[1]);
                                    if (!matcher.find()) {
                                        messages.add("password" + "|" + "E-mail address is incorrect." + System.lineSeparator());

                                    }
                                    break;
                            }
                        } else if (splitStr.length == 1) {
                            messages.add(splitStr[0] + "|" + "The field is incorrect." + System.lineSeparator());

                        }
                    }

                    StringBuffer stringBuffer = new StringBuffer();
                    for (String str : messages) {
                        stringBuffer.append(str);
                    }

                    pw.print(stringBuffer);
                    pw.flush();
                    pw.close();
                } finally {
                    socket.close();
                }

            }
        } finally {
            serverSocket.close();
        }

    }
}
