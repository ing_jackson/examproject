package email;

import model.Email;
import rmiservice.EmailService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.Map;

@WebServlet(name = "email", urlPatterns = {"/email"})
public class EmailController extends HttpServlet {

    rmiservice.EmailService emailServiceRMI;
    socketservice.EmailService emailServiceSocket;

    @Override
    public void init() throws ServletException {
        emailServiceRMI = new rmiservice.EmailService();
        emailServiceSocket = new socketservice.EmailService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        Map<String, String> errorMessages = null;

        switch (req.getParameter("technology")) {
            case "rmi":
                errorMessages = emailServiceRMI.checkEmail((Email) req.getAttribute("email"));
                break;
            case "socket":
                errorMessages = emailServiceSocket.checkEmail((Email) req.getAttribute("email"));
                break;
        }

        if (errorMessages.isEmpty()) {
            requestDispatcher = req.getRequestDispatcher("/success.jsp");
        } else {
            req.setAttribute("messages", errorMessages);
            requestDispatcher = req.getRequestDispatcher("/index.jsp");
        }

        requestDispatcher.forward(req, resp);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {}

    @Override
    public void destroy() {}

}