package email;

import model.Email;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"})
public class Filter implements javax.servlet.Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        Email email = new Email();

        email.setFirstName(req.getParameter("firstname"));
        email.setLastName(req.getParameter("lastname"));
        email.setAge(req.getParameter("age"));
        email.setPhoneNumber(req.getParameter("phone"));
        email.setMailAddress(req.getParameter("email"));
        email.setPassword(req.getParameter("password"));
        req.setAttribute("email", email);

        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {}

}
