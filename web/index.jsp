<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add student</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <form role="form" action="email" method="post">
            <div class="col-lg-6 col-sm-offset-3">

                <div id="legend">
                    <legend class=""><h3>Register e-mail:</h3></legend>
                </div>

                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>RequiredField</strong>
                </div>

                <div class="form-group <c:if test="${messages.containsKey('firstname')}">has-error</c:if> ">
                    <label class="control-label" for="firstname">First Name</label>

                    <div class="input-group">
                        <input type="text" pattern="^[A-Z][A-Za-z]{1,30}" class="form-control" name="firstname" id="firstname"  placeholder="first name" value="<c:if test="${not empty email.getFirstName()}">${email.getFirstName()}</c:if>" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <p class="help-block">Field should contain only letters, without spaces. First letter should be capital.</p>
                </div>

                <div class="form-group <c:if test="${messages.containsKey('lastname')}">has-error</c:if> ">
                    <label class="control-label" for="lastname">Last Name</label>

                    <div class="input-group">
                        <input type="text" pattern="^[A-Z][A-Za-z]{1,30}" class="form-control" name="lastname" id="lastname" placeholder="last name" value="<c:if test="${not empty email.getLastName()}">${email.getLastName()}</c:if>" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <p class="help-block">Field should contain only letters, without spaces. First letter should be capital.</p>
                </div>

                <div class="form-group <c:if test="${messages.containsKey('age')}">has-error</c:if>">
                    <label class="control-label" for="age">Age</label>
                    <div class="input-group">
                        <input type="number" min="1" max="150" step="1" class="form-control" name="age" id="age" placeholder="age" value="<c:if test="${not empty email.getAge()}">${email.getAge()}</c:if>" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <p class="help-block">Field should contain an integer.</p>
                </div>

                <div class="form-group <c:if test="${messages.containsKey('phone')}">has-error</c:if>">
                    <label class="control-label" for="phone">Phone number</label>
                    <div class="input-group">
                        <input type="tel" pattern="[0-9]{3}-[0-9]{1}-[0-9]{2}-[0-9]{2}" class="form-control" name="phone" id="phone" placeholder="phone number" value="<c:if test="${not empty email.getPhoneNumber()}">${email.getPhoneNumber()}</c:if>" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <p class="help-block">Phone number format should be xxx-x-xx-xx.</p>
                </div>

                <div class="form-group <c:if test="${messages.containsKey('email')}">has-error</c:if>">
                    <label class="control-label" for="email">E-mail adress</label>
                    <div class="input-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="e-mail" value="<c:if test="${not empty email.getMailAddress()}">${email.getMailAddress()}</c:if>" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <p class="help-block">Field should start with letter.</p>
                </div>

                <div class="form-group <c:if test="${messages.containsKey('password')}">has-error</c:if>">
                    <label class="control-label" for="password">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="password" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <p class="help-block">Field should contain at least 2 upper-case letters, 3 lower-case letters, 3 digits and 3 other symbols.</p>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Technology:</label>
                    <div class="input-group">
                        <label class="radio-inline"><input type="radio" name="technology" value="rmi" checked> RMI</label>
                        <label class="radio-inline"><input type="radio" name="technology" value="socket"> Socket</label>
                    </div>
                </div>

                <input type="submit" id="submit" value="Register"  class="btn btn-primary pull-right">
                <input type="submit" id="submit-no-validate" value="Register without HTML validation"  formnovalidate class="btn btn-primary pull-left">
            </div>
        </form>
    </div>
</div>
</body>
</html>

